package cz.dejfcold.menu.pojos;

import lombok.Data;

@Data
public class Food {
	private String name;
	private String price;
	private String size;
	private String alergens;
	private FoodType foodType;
}
