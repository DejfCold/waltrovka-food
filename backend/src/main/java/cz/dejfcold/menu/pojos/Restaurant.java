package cz.dejfcold.menu.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class Restaurant {
	private String name;
	private String link;
	private String logo;
}
