package cz.dejfcold.menu.pojos;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Day {
	private String dayName;
	private List<Food> foods = new ArrayList<>();	
}
