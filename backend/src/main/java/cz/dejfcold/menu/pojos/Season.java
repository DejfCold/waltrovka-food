package cz.dejfcold.menu.pojos;

import java.util.List;

import lombok.Data;

@Data
public class Season {
	private List<Food> foodType;
}
