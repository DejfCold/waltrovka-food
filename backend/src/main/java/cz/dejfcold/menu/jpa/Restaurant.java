package cz.dejfcold.menu.jpa;

import lombok.Data;

//@Entity
@Data
public class Restaurant {
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String link;
	private String logo;
}
