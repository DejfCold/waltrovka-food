package cz.dejfcold.menu.jpa;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

//@Entity
@Data
public class DailyMenu {
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long dailyMenuId;
	private String dayName;
	private List<Food> foods = new ArrayList<>();	

}
