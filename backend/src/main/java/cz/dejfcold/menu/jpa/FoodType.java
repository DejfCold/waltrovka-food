package cz.dejfcold.menu.jpa;

import lombok.Data;

//@Entity
@Data
public class FoodType {
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long foodTypeId;
	private String name;
}
