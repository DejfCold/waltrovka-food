package cz.dejfcold.menu.jpa;

import cz.dejfcold.menu.pojos.FoodType;
import lombok.Data;

//@Entity
@Data
public class Food {
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long foodId;
	private String name;
	private String price;
	private String size;
	private String alergens;
	private FoodType foodType;
}
