package cz.dejfcold.menu;

public final class Constants {
	public static final String PUZZLE_SALADS = "puzzle";
	public static final String JOHNSON = "johnson";
	public static final String BISTRO_DYNAMICA = "bistro";
	public static final String PERFECT_CANTEEN = "perfect";
	
	private Constants() {
	}
}
