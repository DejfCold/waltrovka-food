package cz.dejfcold.menu.utils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
	
	private static final List<String> WEEKDAYS = getWeekdays();
	private static final Pattern PRICE_PATTERN = Pattern.compile("\\d+ (Kč|CZK)");
	private static final Pattern ALERGEN_PATTERN = Pattern.compile("\\s{2,}.+\\s{2,}");
	private static final Pattern ALERGEN_PART_PATTERN = Pattern.compile("^(\\d+[,\\s]*)+\\s{2}.*$");
	private static final Pattern SIZE_PATTERN = Pattern.compile("^\\d+\\s\\w");

	public static List<String> string2lines(String string) {

		return Arrays.asList(string.split(System.lineSeparator()));
	}
	public static Optional<String> getPrice(String line) {
		Matcher matcher = PRICE_PATTERN.matcher(line);
		
		return Optional.ofNullable(find(matcher));
		
	}
	private static String find(Matcher matcher) {
		if(matcher.find()) {			
			return matcher.group(0);
		}
		return "";
	}
	
	public static String getName(String line) {
		String priceless = line.replace(getPrice(line).orElseThrow(), "");
		String alergenLess = priceless.replace(getAlergens(priceless), "");
		String sizeless = alergenLess.replace(getSize(alergenLess), "");
		String dashless = sizeless.replace(" - ", "");
		return dashless.strip();
	}
	
	public static String getSize(String line) {
		Matcher matcher = SIZE_PATTERN.matcher(line);
		return find(matcher);
	}
	
	public static String getAlergens(String line) {
		Matcher matcher = ALERGEN_PATTERN.matcher(line);
		return find(matcher).trim();
		
	}
	
	public static boolean isDayName(String line) {
		return WEEKDAYS.stream().anyMatch(wd -> wd.toLowerCase().contentEquals(line.toLowerCase().trim()));

	}
	
	public static boolean isBuffet(String line) {
		return line.contentEquals("TEPLÝ SAMOOBSLUŽNÝ BUFET") ||
				line.contentEquals("Hot self - serve buffet");
	}
	
	public static boolean isSeasonMenu(String line) {
		return line.contains("SEZÓNNÍ MENU") || line.contains("SEASONAL MENU");
	}
	
	private static List<String> getWeekdays() {
		final List<String> cz = Arrays.asList(DateFormatSymbols.getInstance(Locale.forLanguageTag("cs-CZ")).getWeekdays());
		final List<String> en = Arrays.asList(DateFormatSymbols.getInstance(Locale.ENGLISH).getWeekdays());
		List<String> weekdays = new ArrayList<>(); 
		weekdays.addAll(cz);
		weekdays.addAll(en);
		weekdays.removeIf(x -> x.trim().isBlank());
		return Collections.unmodifiableList(weekdays);
	}
	public static boolean isNotFood(String l) {
		return getPrice(l).orElse("").isBlank();
	}
	public static boolean isPartAlergens(String l) {
		Matcher matcher = ALERGEN_PART_PATTERN.matcher(l.strip());
		return !find(matcher).isBlank();
	}
	
}
