package cz.dejfcold.menu.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cz.dejfcold.menu.jpa.Restaurant;

//@Repository
public interface RestaurantRepository extends CrudRepository<Restaurant, Long> {
	public Optional<Restaurant> findByName(@Param("name") String name);
}
