package cz.dejfcold.menu.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.dejfcold.menu.Constants;
import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.pojos.Restaurant;
import cz.dejfcold.menu.services.IMenuProvider;
import cz.dejfcold.menu.services.IParser;
import cz.dejfcold.menu.services.MenuProviderFactory;
import cz.dejfcold.menu.services.ParserFactory;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(MenuController.API_MENU)
public class MenuController {

	private static final String BISTRO_DYNAMICA = "/bistroDynamica";
	private static final String PERFECT_CANTEEN = "/perfectCanteen";
	private static final String PUZZLE_SALADS = "/puzzleSalads";
	private static final String JOHNSON = "/johnson";
	protected static final String API_MENU = "/api/versions/0/restaurants";

	private String[] restaurants;
	private ParserFactory mp;
	private MenuProviderFactory menuProviderFactory;
	private Environment env;
	
	public MenuController(
			@Value("${restaurants}") String[] restaurants,
			@Autowired ParserFactory mp,
			@Autowired MenuProviderFactory menuProviderFactory,
			@Autowired Environment env) {
		this.restaurants = restaurants;
		this.mp = mp;
		this.menuProviderFactory = menuProviderFactory;
		this.env = env;
	}


	@GetMapping
	public List<Restaurant> getRestaurants() {
		List<Restaurant> restaurantList = new ArrayList<>();
		for (String restaurant : restaurants) {
			restaurantList.add(new Restaurant(env.getProperty(restaurant + "_name"),
					API_MENU + env.getProperty(restaurant + "_endpoint"), env.getProperty(restaurant + "_logo")));
		}
		return restaurantList;
	}

	@GetMapping(PERFECT_CANTEEN)
	public List<String> getPerfectCanteen() {
		ArrayList<String> str = new ArrayList<>();
		str.add(API_MENU + PERFECT_CANTEEN + "/cz");
		str.add(API_MENU + PERFECT_CANTEEN + "/en");
		return str;
	}
	
	@GetMapping(PUZZLE_SALADS)
	public List<String> getPuzzleSalads() {
		ArrayList<String> str = new ArrayList<>();
		str.add(API_MENU + PUZZLE_SALADS + "/cz");
		str.add(API_MENU + PUZZLE_SALADS + "/en");
		return str;
	}

	@GetMapping(BISTRO_DYNAMICA)
	public List<String> getBistroDynamica() {
		ArrayList<String> str = new ArrayList<>();
		str.add(API_MENU + BISTRO_DYNAMICA + "/cz");
		str.add(API_MENU + BISTRO_DYNAMICA + "/en");
		return str;
	}

	@GetMapping(JOHNSON)
	public List<String> getJohnson() {
		ArrayList<String> str = new ArrayList<>();
		str.add(API_MENU + JOHNSON + "/cz");
		str.add(API_MENU + JOHNSON + "/en");
		return str;
	}

	@GetMapping(PERFECT_CANTEEN + "/{lang}")
	public List<Day> getPerfectCanteen(@PathVariable("lang") String lang) throws IOException {
		return buildMenu(lang, Constants.PERFECT_CANTEEN);
	}

	@GetMapping(BISTRO_DYNAMICA + "/{lang}")
	public List<Day> getBistroDynamica(@PathVariable("lang") String lang) throws IOException {
		return buildMenu(lang, Constants.BISTRO_DYNAMICA);
	}

	@GetMapping(JOHNSON + "/{lang}")
	public List<Day> getJohnson(@PathVariable("lang") String lang) throws IOException {
		return buildMenu(lang, Constants.JOHNSON);
	}
	
	@GetMapping(PUZZLE_SALADS + "/{lang}")
	public List<Day> getPuzzle(@PathVariable("lang") String lang) throws IOException {
		return buildMenu(lang, Constants.PUZZLE_SALADS);
	}
	
	private List<Day> buildMenu(String lang, String restaurant) {
		var menuProvider = menuProviderFactory.createMenuProvider(restaurant);
		var parser = mp.createParser(restaurant);
		var ret = parser.parse(menuProvider.provide(lang));
		return ret;
	}
}
