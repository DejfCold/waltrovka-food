package cz.dejfcold.menu.services.parsers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.pojos.Food;
import cz.dejfcold.menu.services.IParser;
import cz.dejfcold.menu.utils.StringUtils;

@Service
public class PerfectCanteenParser implements IParser {
	private Food string2food(String line) {
		Food f = new Food();
		f.setAlergens(StringUtils.getAlergens(line));
		f.setName(StringUtils.getName(line));
		f.setPrice(StringUtils.getPrice(line).orElseThrow());
		f.setSize(StringUtils.getSize(line));
		return f;
	}

	public List<Day> parse(Object toParse) {
		if(!(toParse instanceof List)) {
			throw new ClassCastException("toParse should be List<String> but is " + toParse.getClass().getName());
		}
		
		List<String> lines = (List<String>) toParse;
		List<Day> days = new ArrayList<>();
		Day latestDay = null;
		int lineIndex = 0;
		boolean isLastIncomplete = false;
		String lastString = null;
		for(String line : lines) {
			lineIndex++;
			
			//ignore PDF Header, PDF Footer, buffet
			if(isPDFHeader(lineIndex) || isPDFFooter(lines, lineIndex) || isBuffet(line)) {
				continue;
			}
			
			if (isDayName(line)) {
				latestDay = createDay(days, line);
				continue;
			}
			
			if(isLastIncomplete) {
				String concated = lastString + line;
				Food food = string2food(concated);
				latestDay.getFoods().add(food);
				isLastIncomplete = false;
				lastString = null;
				continue;
			}
			
			Food food = string2food(line);
			if(isPriceEmpty(food)) {
				isLastIncomplete = true;
				lastString = line;
				continue;
			} else {
				latestDay.getFoods().add(food);				
			}
		}
		return days;
	}

	private Day createDay(List<Day> days, String line) {
		Day latestDay;
		latestDay = new Day();
		days.add(latestDay);
		latestDay.setDayName(line.strip());
		return latestDay;
	}

	private boolean isPriceEmpty(Food f) {
		return f.getPrice().isBlank();
	}

	private boolean isDayName(String l) {
		return StringUtils.isDayName(l.strip());
	}

	private boolean isBuffet(String l) {
		return StringUtils.isBuffet(l.strip());
	}

	private boolean isPDFFooter(List<String> lines, int index) {
		return index-1+8 >= lines.size();
	}

	private boolean isPDFHeader(int index) {
		return index-1<4;
	}
}
