package cz.dejfcold.menu.services.parsers;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.pojos.Food;
import cz.dejfcold.menu.pojos.FoodType;
import cz.dejfcold.menu.services.IParser;

@Service
public class JohnsonParser implements IParser {
	public List<Day> parse(Object toParse) {
		if(!(toParse instanceof Document)) {
			throw new IllegalArgumentException("toParse should be <Document> but is <" + toParse.getClass().getName()+">");
		}
		List<Day> dayList = new ArrayList<>();
		Document doc = (Document)toParse;
		Elements days = getDays(doc);
		for( Element day : days) {
			Day d = new Day();
			d.setDayName(getDayName(day));
			dayList.add(d);
			
			Elements items = day.select("tr:gt(2)");
			for(Element item : items) {
				String name = getName(item);
				String price = getPrice(item);
				String foodTypeName = getFoodType(item);
				String size = getSize(item);
				
				FoodType ft= new FoodType(foodTypeName);
				Food f = new Food();
				f.setAlergens(null);
				f.setName(name);
				f.setPrice(price);
				f.setSize(size);
				f.setFoodType(ft);
				d.getFoods().add(f);
			}
		}
		return dayList;
	}

	private String getDayName(Element day) {
		return day.select("h2").get(0).ownText().strip().split(" ")[0];
	}

	private Elements getDays(Document doc) {
		return doc.select(".today-menu");
	}

	private String getSize(Element item) {
		return item.select(".vaha").first().ownText().strip();
	}

	private String getFoodType(Element item) {
		return item.select(".popis").first().text().strip();
	}

	private String getPrice(Element item) {
		return item.select(".pismo").first().ownText().strip();
	}

	private String getName(Element item) {
		return item.select(".popisJidla").first().ownText().strip();
	}
}
