package cz.dejfcold.menu.services.parsers;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.pojos.Food;
import cz.dejfcold.menu.pojos.FoodType;
import cz.dejfcold.menu.services.IParser;

@Service
public class PuzzleSaladsParser implements IParser {
	
	public List<Day> parse(Object toParse) {
		if(!(toParse instanceof Document)) {
			throw new IllegalArgumentException("toParse should be <Document> but is <" + toParse.getClass().getName()+">");
		}
		
		List<Day> dayList = new ArrayList<>();
		Document doc = (Document)toParse;
		Element menu = getMenuElement(doc);
		String dayName = getDayName(menu);
		Day day = createDay(dayName);
		dayList.add(day);
		List<Food> foods = day.getFoods();
		
		for(Element foodByType : getFoodsByType(menu)) {
			FoodType foodType = getFoodType(foodByType);
			
			for(Element foodElement : getFoodElements(foodByType)) {
				Food food = new Food();
				String foodName = getFoodName(foodElement);
				String price = getFoodPrice(foodElement);
				String allergens = getFoodAllergens(foodElement);
				food.setFoodType(foodType);
				food.setName(foodName);
				food.setPrice(price);
				food.setAlergens(allergens);
				foods.add(food);
			}
			
		}
		return dayList;
	}

	private String getFoodAllergens(Element food) {
		return food.selectFirst("small").ownText().strip();
	}

	private String getFoodPrice(Element food) {
		return food.selectFirst("span>i").ownText().strip();
	}

	private String getFoodName(Element food) {
		return food.ownText();
	}

	private Elements getFoodElements(Element foodByType) {
		return foodByType.select(".content-content");
	}

	private FoodType getFoodType(Element foodByType) {
		return new FoodType(getTypeName(foodByType));
	}

	private String getTypeName(Element foodByType) {
		return foodByType.select("h3.title-content").first().text().strip();
	}

	private Elements getFoodsByType(Element menu) {
		return menu.select(".about-content-item");
	}

	private Day createDay(String dayName) {
		Day day = new Day();
		day.setDayName(dayName);
		return day;
	}

	private Element getMenuElement(Document doc) {
		return doc.select("#denni-menu").get(0);
	}

	private String getDayName(Element menu) {
		return menu.select(".heading-section-1").get(1).select("h3").first().text().strip().split(" ")[3];
	}
}
