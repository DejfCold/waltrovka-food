package cz.dejfcold.menu.services;

import java.util.List;

import cz.dejfcold.menu.pojos.Day;

public interface IParser {
	public List<Day> parse (Object toParse);
}
