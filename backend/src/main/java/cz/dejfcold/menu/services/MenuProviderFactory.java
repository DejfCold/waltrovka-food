package cz.dejfcold.menu.services;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import cz.dejfcold.menu.Constants;
import cz.dejfcold.menu.services.providers.BistroDynamicaMenuProvider;
import cz.dejfcold.menu.services.providers.JohnsonMenuProvider;
import cz.dejfcold.menu.services.providers.PerfectCanteenMenuProvider;
import cz.dejfcold.menu.services.providers.PuzzleSaladsMenuProvider;

@Service
public class MenuProviderFactory {
	public static final String PUZZLE_SALADS = "puzzle";
	public static final String JOHNSON = "johnson";
	public static final String BISTRO_DYNAMICA = "bistro";
	public static final String PERFECT_CANTEEN = "perfect";
	
	private Environment env;
	private PdfExtractor extractor;
	
	public MenuProviderFactory(
			@Autowired Environment env,
			@Autowired PdfExtractor extractor) {
		this.env = env;
		this.extractor = extractor;
	}
	
	private Map<String, Supplier<IMenuProvider>> providers = new HashMap<>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3459314908084933492L;
		{
			put(PERFECT_CANTEEN, () -> new PerfectCanteenMenuProvider(env, extractor));
			put(BISTRO_DYNAMICA, () -> new BistroDynamicaMenuProvider(env, extractor));
			put(JOHNSON, () -> new JohnsonMenuProvider(env));
			put(PUZZLE_SALADS, () -> new PuzzleSaladsMenuProvider(env));
		}
	};
	

	/**
	 * Providers can be found in {@link Constants}
	 * @param menuProviderType
	 * @return
	 */
	public IMenuProvider createMenuProvider(String menuProviderType) {
		return providers.get(menuProviderType).get();
	}
}
