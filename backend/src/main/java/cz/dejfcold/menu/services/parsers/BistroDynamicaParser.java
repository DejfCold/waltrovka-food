package cz.dejfcold.menu.services.parsers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.pojos.Food;
import cz.dejfcold.menu.pojos.FoodType;
import cz.dejfcold.menu.services.IParser;
import cz.dejfcold.menu.utils.StringUtils;

@Service
public class BistroDynamicaParser implements IParser {
	private Food string2food(String line) {
		Food f = new Food();
		f.setAlergens(StringUtils.getAlergens(line));
		f.setName(StringUtils.getName(line));
		f.setPrice(StringUtils.getPrice(line).orElseThrow());
		f.setSize(StringUtils.getSize(line));
		return f;
	}
	
	public List<Day> parse(Object toParse) {
		if(!(toParse instanceof List)) {
			throw new ClassCastException("toParse should be List<String> but is " + toParse.getClass().getName());
		}
		
		List<String> lines = (List<String>)toParse;
		List<Day> days = new ArrayList<>();
		Day latest = null;
		boolean isSeason = false;
		FoodType foodType = null;
		int lineIndex = 0;
		for(String line : lines) {
			lineIndex++;
			
			//ignore PDF Header, PDF Footer, buffet
			if(isPDFHeader(lineIndex) || isPDFFooter(lines, lineIndex) || isBuffet(line)) {
				continue;
			}
			
			if (isDayName(line)) {
				latest = new Day();
				days.add(latest);
				latest.setDayName(getDayName(line));
				continue;
			}
			
			if(isSeasonMenu(line)) {
				isSeason = true;
				latest = new Day();
				days.add(latest);
				continue;
			}
			
			if(isSeason && isNonFood(line)) {
				foodType = new FoodType(getDayName(line));
				continue;
			}
			
			Food food = string2food(line);
			food.setFoodType(foodType);
			latest.getFoods().add(food);
		}
		return days;
	}

	private String getDayName(String line) {
		return line.strip().split(" ")[0];
	}

	private boolean isNonFood(String l) {
		return StringUtils.isNotFood(l);
	}

	private boolean isSeasonMenu(String l) {
		return StringUtils.isSeasonMenu(l);
	}

	private boolean isDayName(String l) {
		var s = getDayName(l);
		return StringUtils.isDayName(s);
	}

	private boolean isBuffet(String l) {
		return StringUtils.isBuffet(l.strip());
	}

	private boolean isPDFFooter(List<String> lines, int index) {
		return index-1+8 >= lines.size();
	}

	private boolean isPDFHeader(int index) {
		return index-1<4;
	}
}
