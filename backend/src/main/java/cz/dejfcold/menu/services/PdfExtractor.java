package cz.dejfcold.menu.services;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Service;

@Service
public class PdfExtractor {
	public String extractPdf2String(String path) throws IOException {
		File f = new File(path);
		return extractPdf2String(f);
	}
	
	public String extractPdf2String(File file) throws IOException {
		PDFTextStripper pdfStripper = new PDFTextStripper();
		try(PDDocument pdDoc = PDDocument.load(file);) {			
			String parsedText = pdfStripper.getText(pdDoc);
			return parsedText;
		}
	}
}
