package cz.dejfcold.menu.services.providers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.jsoup.UncheckedIOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import cz.dejfcold.menu.services.IMenuProvider;
import cz.dejfcold.menu.services.PdfExtractor;
import cz.dejfcold.menu.utils.StringUtils;

public class PerfectCanteenMenuProvider implements IMenuProvider {
	private Environment env;
	private PdfExtractor extractor;
	
	public PerfectCanteenMenuProvider(
			@Autowired Environment env,
			@Autowired PdfExtractor extractor) {
		this.env = env;
		this.extractor = extractor;
	}

	@Override
	public Object provide(String language) {
		String url = env.getProperty("perfect_" + language);
		List<String> text = tryReadPdfFromUrl(language, url);
		return text;
	}
	
	private List<String> tryReadPdfFromUrl(String language, String url) {
		try {
			return readPdfFromUrl(language, url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}
	
	private List<String> readPdfFromUrl(String language, String url) throws MalformedURLException, IOException {
		URL u = new URL(url);
		File f = File.createTempFile(language+"_", Long.toHexString(new Random().nextLong()));
		if(!f.canWrite()) {
			throw new UncheckedIOException(new IOException("cant write to a temp file: " + f.getAbsolutePath()));
		}
		f.deleteOnExit();
		FileUtils.copyURLToFile(u, f);
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String(f));
		f.delete();
		return text;
	}

}
