package cz.dejfcold.menu.services.providers;

import java.io.IOException;
import java.io.UncheckedIOException;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import cz.dejfcold.menu.services.IMenuProvider;

@Service
public class PuzzleSaladsMenuProvider implements IMenuProvider {

	private Environment env;
	
	public PuzzleSaladsMenuProvider(@Autowired Environment env) {
		this.env = env;
	}

	@Override
	public Object provide(String language) {
		String url = env.getProperty("puzzle_" + language);
		try {
			return Jsoup.connect(url).get();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}


}
