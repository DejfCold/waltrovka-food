package cz.dejfcold.menu.services;

public interface IMenuProvider {
	Object provide(String language);
}
