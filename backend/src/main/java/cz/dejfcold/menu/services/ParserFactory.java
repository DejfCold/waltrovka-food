package cz.dejfcold.menu.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;

import org.apache.commons.io.FileUtils;
import org.jsoup.UncheckedIOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.services.parsers.BistroDynamicaParser;
import cz.dejfcold.menu.services.parsers.JohnsonParser;
import cz.dejfcold.menu.services.parsers.PerfectCanteenParser;
import cz.dejfcold.menu.services.parsers.PuzzleSaladsParser;
import cz.dejfcold.menu.utils.StringUtils;

@Service
public class ParserFactory {
	public static final String PUZZLE_SALADS = "puzzle";
	public static final String JOHNSON = "johnson";
	public static final String BISTRO_DYNAMICA = "bistro";
	public static final String PERFECT_CANTEEN = "perfect";
	private Map<String, Supplier<IParser>> parsers = new HashMap<>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3459314908084933492L;
		{
			put(PERFECT_CANTEEN, PerfectCanteenParser::new);
			put(BISTRO_DYNAMICA, BistroDynamicaParser::new);
			put(JOHNSON, JohnsonParser::new);
			put(PUZZLE_SALADS, PuzzleSaladsParser::new);
		}
	};
	@Autowired
	private Environment env;
	
	@Autowired
	private PdfExtractor extractor;
	
	@Autowired 
	private BistroDynamicaParser bd;
	
	@Autowired
	private PerfectCanteenParser pc;
	
	@Autowired
	private JohnsonParser j;
	
	@Autowired
	private PuzzleSaladsParser ps;
	
	public IParser createParser(String parserType) {
		return parsers.get(parserType).get();
	}
	
	@Deprecated
	public List<Day> getPerfectCanteen(String language) throws IOException {
		String url = env.getProperty("perfect_" + language);
		List<String> text = Collections.unmodifiableList(readPdfFromUrl(language, url));
		return pc.parse(text);
	}

	private List<String> readPdfFromUrl(String language, String url) throws MalformedURLException, IOException {
		URL u = new URL(url);
		File f = File.createTempFile(language+"_", Long.toHexString(new Random().nextLong()));
		if(!f.canWrite()) {
			throw new UncheckedIOException("cant write to a temp file: " + f.getAbsolutePath());
		}
		f.deleteOnExit();
		FileUtils.copyURLToFile(u, f);
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String(f));
		f.delete();
		return text;
	}
	
	@Deprecated
	public List<Day> getBistroDynamica(String language) throws IOException {
		String url = env.getProperty("bistro_" + language);
		List<String> text = readPdfFromUrl(language, url);
		return bd.parse(text);
	}

	@Deprecated
	public List<Day> getJohnson(String lang) throws IOException {
		String url = env.getProperty("johnson_" + lang);
		return j.parse(url);
	}
	
	@Deprecated
	public List<Day> getPuzzle(String lang) throws IOException {
		String url = env.getProperty("puzzle_" + lang);
		return ps.parse(url);
	}
}
