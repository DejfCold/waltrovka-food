package cz.dejfcold.menu.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.dejfcold.menu.jpa.Restaurant;
import cz.dejfcold.menu.repositories.RestaurantRepository;

//@Service
public class RestaurantService {
	@Autowired 
	private RestaurantRepository repository;
	
	public Iterable<Restaurant> getRestaurants() {
		return repository.findAll();
	}
	
	public Optional<Restaurant> getRestaurantByName(String name) {
		return repository.findByName(name);
	}
}
