package cz.dejfcold.menu.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StringUtilsTest {
	
	@Test
	public void isPartAlergensTest() {
		assertTrue(StringUtils.isPartAlergens("10, 11  15Kč"));
	}
}
