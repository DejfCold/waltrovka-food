package cz.dejfcold.menu.services;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.services.parsers.JohnsonParser;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JohnsonParserTest {
	@Autowired
	private JohnsonParser jp;
	
	@Test
	public void testCZ() throws IOException {
		String url = "http://johnson-johnson.portal.sodexo.cz/cs/jidelni-listek-na-cely-tyden";
		List<Day> days = jp.parse(Jsoup.connect(url).get());
		log.info(days.toString());
	}
}
