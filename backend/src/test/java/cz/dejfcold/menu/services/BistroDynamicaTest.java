package cz.dejfcold.menu.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.dejfcold.menu.services.PdfExtractor;
import cz.dejfcold.menu.services.parsers.BistroDynamicaParser;
import cz.dejfcold.menu.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BistroDynamicaTest {
	
	@Autowired
	private PdfExtractor extractor;
	
	@Test
	public void testCZ() throws FileNotFoundException, IOException {
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String("src/test/resources/bd-cz.pdf"));
		log.info(new BistroDynamicaParser().parse(text).toString().replaceAll("Food\\(", "\r\n\tFood(").replaceAll("Day\\(", "\r\nDay("));
	}
	
	@Test
	public void testEN() throws FileNotFoundException, IOException {
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String("src/test/resources/bd-en.pdf"));
		System.out.println(new BistroDynamicaParser().parse(text));

	}
	
	@Test
	public void testCZ2020() throws FileNotFoundException, IOException {
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String("src/test/resources/bd-cz-2020.pdf"));
		log.info(new BistroDynamicaParser().parse(text).toString().replaceAll("Food\\(", "\r\n\tFood(").replaceAll("Day\\(", "\r\nDay("));
	}
	
	@Test
	public void testEN2020() throws FileNotFoundException, IOException {
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String("src/test/resources/bd-en-2020.pdf"));
		System.out.println(new BistroDynamicaParser().parse(text));

	}
}
