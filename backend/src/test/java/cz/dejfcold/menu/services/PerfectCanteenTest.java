package cz.dejfcold.menu.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.dejfcold.menu.services.PdfExtractor;
import cz.dejfcold.menu.services.parsers.PerfectCanteenParser;
import cz.dejfcold.menu.utils.StringUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PerfectCanteenTest {
	
	@Autowired
	private PdfExtractor extractor;
	
	@Test
	public void testCZ() throws FileNotFoundException, IOException {
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String("src/test/resources/pc-cz.pdf"));
		System.out.println(new PerfectCanteenParser().parse(text));
	}
	
	@Test
	public void testEN() throws FileNotFoundException, IOException {
		List<String> text = StringUtils.string2lines(extractor.extractPdf2String("src/test/resources/pc-en.pdf"));
		System.out.println(new PerfectCanteenParser().parse(text));

	}
}
