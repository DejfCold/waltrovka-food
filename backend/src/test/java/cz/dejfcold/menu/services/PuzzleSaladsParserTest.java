package cz.dejfcold.menu.services;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.dejfcold.menu.pojos.Day;
import cz.dejfcold.menu.services.parsers.PuzzleSaladsParser;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PuzzleSaladsParserTest {
	@Autowired
	private PuzzleSaladsParser ps;
	
	@Test
	public void testCZ() throws IOException {
		String url = "https://puzzlesalads.cz/dashboard/provozovna/?idp=7";
		List<Day> days = ps.parse(Jsoup.connect(url).get());
		log.info(days.toString());
	}
}
