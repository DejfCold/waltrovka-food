package cz.dejfcold.menu.services;

import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.dejfcold.menu.services.PdfExtractor;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PdfExtractorTest {
	
	@Autowired
	private PdfExtractor extractor;
	
	@Test
	public void testPCCZ() throws FileNotFoundException, IOException {
		assertNotNull(extractor.extractPdf2String("src/test/resources/pc-cz.pdf"));
	}
	
	@Test
	public void testPCEN() throws FileNotFoundException, IOException {
		assertNotNull(extractor.extractPdf2String("src/test/resources/pc-en.pdf"));
	}
	
	@Test
	public void testBDCZ() throws FileNotFoundException, IOException {
		String bd = extractor.extractPdf2String("src/test/resources/bd-cz.pdf");
		log.info(bd);
		assertNotNull(bd);
	}
	
	@Test
	public void testBDEN() throws FileNotFoundException, IOException {
		String bd = extractor.extractPdf2String("src/test/resources/bd-en.pdf");
		log.info(bd);
		assertNotNull(bd);
	}
}
