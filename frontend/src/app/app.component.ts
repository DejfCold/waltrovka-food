import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private service: ApiService) {}
  title = 'menu-parser-fe';

  resetLang() {
    localStorage.removeItem('language');
  }
  showLangChange() {
    return null !== localStorage.getItem('language');
  }

  lang() {
    if (localStorage.getItem('language') === 'cz') {
      return 'Změnit jazyk';
    }
    return 'Change language';
  }
}
