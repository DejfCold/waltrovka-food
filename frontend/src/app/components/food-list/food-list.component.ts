import { Component, OnInit } from '@angular/core';
import { StateService } from 'src/app/services/state.service';
import { ApiService } from 'src/app/services/api.service';
import { of, Observable, PartialObserver } from 'rxjs';

@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.css']
})
export class FoodListComponent implements OnInit {

  constructor(private state: StateService, private service: ApiService) { }

  private menu: Day[];
  ngOnInit() {
    const state = this.state.getState();
    this.service.getWeeklyMenu(window.location.href.split('/').pop()).subscribe({
      next: (x) => {this.menu = x; }
    });


  }

  nonEmpty() {
    if (this.menu !== undefined) {
      return this.menu.filter(d => d.dayName !== null);
    }
  }
  hasOther() {
    if (this.isEmpty()) {
      return false;
    }
    if (this.menu.filter(d => d.dayName === null).length === 0 ) {
      return false;
    }
    return true;
  }
  otherMenuText() {
    return localStorage.getItem('language') === 'en' ? 'Other menu' : 'Jiné menu';
  }
  otherFood() {
    if (!this.hasOther()) {
      return;
    }

    const foods = new Map<String, any[]>();
    this.menu.filter(d => d.dayName === null)[0].foods.forEach(d => {
      const food: {name: String, price: String, size: String, alergens: String} = {
        name: d.name,
        price: d.price,
        size: d.size,
        alergens: d.alergens
      };
      if (foods.has(d.foodType.name)) {
        foods.get(d.foodType.name).push(food);
      } else {
        foods.set(d.foodType.name, [food]);
      }
    });
    return foods;
  }
  isEmpty() {
    return this.menu === undefined;
  }

}
