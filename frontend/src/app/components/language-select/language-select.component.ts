import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-language-select',
  templateUrl: './language-select.component.html',
  styleUrls: ['./language-select.component.css']
})
export class LanguageSelectComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    if (null !== localStorage.getItem('language')) {
      this.router.navigate(['/restaurants']);
    }
  }

  setLanguage(lang: string) {
    localStorage.setItem('language', lang);
    this.router.navigate(['/restaurants']);
  }

}
