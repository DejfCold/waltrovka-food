import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { StateService } from 'src/app/services/state.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {

  constructor(private service: ApiService, private state: StateService) {}
  restaurants;
  ngOnInit(): void {
    this.service.getRestaurants().subscribe(x => {
      console.log(x);
      this.restaurants = x;
    });
  }

  pass(restaurant: Restaurant) {
    this.state.saveState(restaurant);
  }

  shortName(restaurant: Restaurant) {
    const a = restaurant.link.split('/');
    return a[a.length - 1];
  }

}
