import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public readonly API_URL = environment.url;
  private readonly API_INIT = '/api/versions/0/restaurants';

  constructor(private http: HttpClient) { }

  public getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>(this.API_URL + this.API_INIT);
  }

  public getWeeklyMenu(shortName: string): Observable<Day[]> {
    return this.http.get<Day[]>(this.API_URL + this.API_INIT + '/' + shortName + '/' + localStorage.getItem('language'));
  }
}
