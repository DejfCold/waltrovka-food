import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  constructor() { }
  private state: any;
  saveState(state: any) {
    this.state = state;
  }

  getState() {
    const st = this.state;
    this.state = undefined;
    return st;
  }
}
