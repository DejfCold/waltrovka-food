import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RestaurantListComponent } from './components/restaurant-list/restaurant-list.component';
import { FoodListComponent } from './components/food-list/food-list.component';
import { LanguageSelectComponent } from './components/language-select/language-select.component';
import { LoadingWheelComponent } from './components/loading-wheel/loading-wheel.component';

const routes: Routes = [
  { path: '', redirectTo: '/language', pathMatch: 'full'},
  { path: 'restaurants', component: RestaurantListComponent, canActivate: [LanguageGuard]},
  { path: 'restaurants/:restaurant', component: FoodListComponent, canActivate: [LanguageGuard]},
  { path: 'language', component: LanguageSelectComponent, canActivate: [LanguageGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    RestaurantListComponent,
    FoodListComponent,
    LanguageSelectComponent,
    LoadingWheelComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
